import { Component, OnInit } from '@angular/core';
import { OrdersService, Order } from '../_services/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
    orders: Order[] = [];

    constructor(
        private serviceOrders: OrdersService,
    ) { }

    ngOnInit() {
        this.orders = this.serviceOrders.Get();
    }

    ClickDelete(o: Order) {
        const index = this.orders.indexOf(o);
        if (index != -1) {
            this.orders.splice(index, 1);
        }
        return false;
    }
}
