import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';

@Injectable()
export class TimesService {
    private url = 'https://www.mocky.io/v2/';

    constructor(
        private jsonp: Jsonp,
    ) { }

    Get(id: string) {
        return this.jsonp.request(`${this.url}${id}?mocky-delay=700ms&callback=JSONP_CALLBACK`);
    }
}
