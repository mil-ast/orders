import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';

@Injectable()
export class CitiesService {
    private url = 'https://www.mocky.io/v2/5b34c0d82f00007400376066?mocky-delay=700ms';

    constructor(
        private jsonp: Jsonp,
    ) { }

    Get() {
        return this.jsonp.request(`${this.url}&callback=JSONP_CALLBACK`);
    }
}
