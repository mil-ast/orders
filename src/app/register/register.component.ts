import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CitiesService } from './services/cities.service';
import { TimesService } from './services/times.service';
import { OrdersService, Order } from '../_services/orders.service';

import * as moment from 'moment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    public cities: any[] = [];
    public dates: any[] = [];
    public times: any[] = [];
    public times_raw: any = null;
    nowDay: moment.Moment = moment();

    confirm: boolean = false;
    public reactiveForm: FormGroup;

    constructor(
        private serviceOrders: OrdersService,
        private serviceCities: CitiesService,
        private timesCities: TimesService,
        private fb: FormBuilder,
    ) {
		this.reactiveForm = this.fb.group({
			city: [{}, Validators.required],
			date: [null, [Validators.required, Validators.pattern(/^[\d]{4}\-[\d]{2}\-[\d]{2}$/)]],
			time: [null, [Validators.required, Validators.pattern(/^[\d]{4}\-[\d]{2}\-[\d]{2}\s[\d]{2}:[\d]{2}:[\d]{2}$/)]],
			phone: [null, [Validators.required, Validators.pattern(/^[\+7]+\([\d]{3}\)[\d]{3}\-[\d]{2}\-[\d]{2}$/)]],
			name: [null, [Validators.required, Validators.minLength(3)]],
        });
    }

    ngOnInit() {
        this.reactiveForm.disable();
        
        const req = this.serviceCities.Get();
        req.subscribe((res: any) => {
            const data = res.json().cities;
            if (!Array.isArray(data) || data.length === 0) {
                return;
            }

            this.reactiveForm.patchValue({
                city : data[0],
                //date : this.nowDay.format('YYYY-MM-DD'),
            });

            this.cities = data;
        }, (err) => {
            console.error(err);
        }, () => {
            this.reactiveForm.enable();
            this.selectCity();
        });
    }

    // выбрали другой город
    ChangeCity() {
        this.selectCity();
    }
    // выбрали дату
    ChangeDate() {
        const now_time = moment().format('HH:mm:ss');
        this.nowDay = moment(`${this.reactiveForm.value.date} ${now_time}`);
        this.updateTimes();
    }

    /**
     * Получить список доступных дней и временных промежутков по ID города
     */
    private selectCity() {
        this.dates = [];
        this.times = [];

        this.reactiveForm.disable();

        const req = this.timesCities.Get(this.reactiveForm.value.city.id);
        req.subscribe((res: any) => {
            const data = res.json().data||{};
            this.times_raw = data;

            this.updateTimes();
        }, (err) => {
            console.error(err);
        }, () => {
            this.reactiveForm.enable();
        });
    }

    private updateTimes() {
        const data = this.times_raw||{};
        const dayNow = this.nowDay.format('YYYY-MM-DD');

        this.times = [];
        this.dates = [];

        this.reactiveForm.patchValue({
            time : null,
        });

        for (let d in data) {
            let count_times: number = 0;

            for (let key in data[d]) {
                if (data[d][key].is_not_free) {
                    continue;
                }

                count_times++;

                if (d === dayNow) {
                    this.times.push(data[d][key]);

                    const time_begin = moment(data[d][key].date);
                    if (time_begin.isAfter(this.nowDay)) {
                        continue;
                    }

                    const time_end = moment(`${data[d][key].day} ${data[d][key].end}:00`);
                    if (time_end.isBefore(this.nowDay)) {
                        continue;
                    }

                    this.reactiveForm.patchValue({
                        date : d,
                        time : data[d][key].date
                    });
                }
            }
            // добавляем только даты с имеющими свободное время
            if (count_times > 0) {
                this.dates.push(d);
            }
        }
    }

    /**
     * отправляем форму
     */
    Submit() {
        this.nowDay = moment();
        // создаем новый заказ
        const order = new Order(
            this.reactiveForm.value.city,
            this.reactiveForm.value.date,
            this.reactiveForm.value.time,
            this.reactiveForm.value.phone,
            this.reactiveForm.value.name,
        );
        // добавим запись в глобальный сервис
        this.serviceOrders.Push(order);

        this.reactiveForm.patchValue({
            date : this.nowDay.format('YYYY-MM-DD'),
            time : null,
            phone : null,
            name : null,
        });

        this.SwitchConfirm(true);

        return false;
    }

    SwitchConfirm(event: boolean) {
        this.confirm = event;
        return false;
    };
}
