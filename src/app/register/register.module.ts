import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// сервисы
import { CitiesService } from './services/cities.service';
import { TimesService } from './services/times.service';

@NgModule({
  imports: [
 	BrowserModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    
  ],
  providers : [
    CitiesService,
    TimesService,
  ],
})
export class RegisterModule { }
