import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appPhone]'
})
export class PhoneDirective {
    private mask = /\+7\(([0-9]*)[\)]*([0-9]*)[\-]*([0-9]*)[\-]*([0-9]*)/;

    @HostListener('change', ['$event']) onChange(e) {
        this.render(e);
    }
    @HostListener('keyup', ['$event']) onKeyUp(e) {
        this.render(e);
    }
    @HostListener('focus', ['$event']) onFocus(e) {
        if (this.el.nativeElement.value == '') {
            this.el.nativeElement.value = '+7(';
        }
    }

    constructor(
        private el: ElementRef,
    ) { }

    render(event) {
        const value = this.el.nativeElement.value||'';
        const parsed = value.match(this.mask);

        switch (event.key) {
        case '-':case '(':case ')':case '+':
            this.el.nativeElement.value = this.el.nativeElement.value.substr(0, this.el.nativeElement.value.length - 1);
        break;
        }

        if (parsed === null) {
            this.el.nativeElement.value = `+7(${this.el.nativeElement.value}`;
            return;
        }

        if (parsed[4] !== '') {
            this.el.nativeElement.value = `+7(${parsed[1]})${parsed[2]}-${parsed[3]}-${parsed[4]}`;
        } else if (parsed[3] !== '') {
            this.el.nativeElement.value = `+7(${parsed[1]})${parsed[2]}-${parsed[3]}`;
            if (parsed[3].length == 2) {
                this.el.nativeElement.value = this.el.nativeElement.value.concat('-');
            }
        } else if (parsed[2] !== '') {
            this.el.nativeElement.value = `+7(${parsed[1]})${parsed[2]}`;
            if (parsed[2].length == 3) {
                this.el.nativeElement.value = this.el.nativeElement.value.concat('-');
            }
        } else if (parsed[1] !== '') {
            this.el.nativeElement.value = `+7(${parsed[1]}`;
            if (parsed[1].length == 3) {
                this.el.nativeElement.value = this.el.nativeElement.value.concat(')');
            }
        }
    }
}