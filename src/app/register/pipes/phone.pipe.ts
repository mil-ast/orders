import {Pipe} from '@angular/core';

@Pipe({
    name: 'phoneFormat'
})
export class PhonePipe {
    transform(val: string) {
        if (val.length != 11) {
            return val;
        }

        let responce = '+'.concat(val[0], '(');
        responce = responce.concat(val.substr(1,3), ')');
        responce = responce.concat(val.substr(4,3), '-');
        responce = responce.concat(val.substr(7,2), '-', val.substr(9,2));

        return responce;
    }
}