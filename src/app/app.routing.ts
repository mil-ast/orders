import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { OrdersComponent } from './orders/orders.component';

const appRoutes: Routes = [
  { path: '', component: RegisterComponent },
  { path: 'orders', component: OrdersComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}