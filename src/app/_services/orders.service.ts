import { Injectable } from '@angular/core';

@Injectable()
export class OrdersService {
	private orders: Order[] = [];

	constructor() {
		let city: CityInterface = {id : '111', name: 'просп. 60-летия Октября, 10А', address : 'Кирова 99', phones: ['74956633931'], price : 1200};
		this.Push(new Order(
			city,
			'2018-06-28',
			'2018-06-28 10:00:00',
			'+7(495)663‑39-31',
			'Чубайс Анатолий',
		));

		city = {id : '222', name: 'Москва', address : 'Пятницкая, 2/38, 1', phones: ['78005555550'], price : 165000};
		this.Push(new Order(
			city,
			'2018-07-01',
			'2018-07-01 14:00:00',
			'+7(926)311-22-33',
			'Герман Греф',
		));
	}

	Push(order: Order) {
		this.orders.unshift(order);
	}

	Get() {
		return this.orders;
	}
}

export interface CityInterface {
	id: string;
	name: string;
	address: string;
	phones?: string[];
	price?: number;
}

export class Order {
	city: CityInterface;
	date: string;
	time: string;
	phone: string;
	name: string;

	constructor(city: CityInterface, date: string, time: string, phone: string, name: string) {
		this.city = city;
		this.date = date;
		this.time = time;
		this.phone = phone;
		this.name = name;
	}
}