import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { JsonpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { OrdersComponent } from './orders/orders.component';
// директивы
import { PhoneDirective } from './register/directives/phone.directive' 
// пайпы
import { PhonePipe } from './register/pipes/phone.pipe';
// глобавльные сервисы
import { OrdersService } from './_services/orders.service';
// модули
import { AppRoutingModule } from './app.routing';
import { RegisterModule } from './register/register.module';

import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
registerLocaleData(localeRu, 'ru');

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    OrdersComponent,
    PhoneDirective,
    PhonePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    JsonpModule,
    FormsModule,
    ReactiveFormsModule,

    RegisterModule,
  ],
  providers: [
    OrdersService,
    {provide: LOCALE_ID, useValue: 'ru-RU'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
